#!/bin/bash

set -e

if ! [ -x "$(command -v jq)" ]; then
    read -r -p "jq is not installed. Do you want to install it? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            sudo dnf install -y jq
            ;;
        *)
            echo "Please install jq manually and re-execute this script."
            exit 0
            ;;
    esac
fi

if ! [ -x "$(command -v npm)" ]; then
    read -r -p "npm is not installed. Do you want to install it? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            sudo dnf install -y gcc-c++ make
            curl -sL https://rpm.nodesource.com/setup_10.x | sudo -E bash -
            sudo dnf install -y nodejs
            ;;
        *)
            echo "Please install npm manually and re-execute this script."
            exit 0
            ;;
    esac
fi

if ! [ -x "$(command -v nativefier)" ]; then
    read -r -p "Nativefier is not installed. Do you want to install it? [y/N] " response
    case "$response" in
        [yY][eE][sS]|[yY]) 
            sudo npm install nativefier -g
            ;;
        *)
            echo "Please install nativefier manually and re-execute this script."
            exit 0
            ;;
    esac
fi

echo "Packaging 1password..."

nativefier --name "1Password" --disable-context-menu --disable-dev-tools \
    --icon "icons/1password.png" "https://my.1password.com/"

echo "1password packaged. Installing desktop entry..."

package_file=1-password-linux-x64/resources/app/package.json
wmclass=$(cat $package_file | jq '.name')

dotdesktop=/home/$USER/.local/share/applications/1password.desktop
cp 1password.desktop $dotdesktop

sed -i -e "s|ICON|$(pwd)/icons/1password.png|g" $dotdesktop
sed -i -e "s|PATH|$(pwd)/1-password-linux-x64/1-password|g" $dotdesktop
sed -i -e "s|ID|$wmclass|g" $dotdesktop

echo "1password succesfully installed."

exit 0